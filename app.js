const app = require('express')();
const mongoose = require('mongoose')
const logger = require('morgan')
const bodyParser = require('body-parser')
//const routes = require('./routes')
const config = require('./config')
const user = require('./model/user')
const userAuth = require('./model/userAuth');
const chat = require('./model/chat');
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var FCM = require('fcm-node');

const redis = require('socket.io-redis');

//var redis = require('redis');
var client = '';


//const app = express();
app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//random chat------------------
var numb = 1;

users = [];
strangers = [];
stranger = 'dddd';

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
})


io.on('connection', function (socket) {

    console.log('a user connect with socket id=' + socket.id + "!");
    let id=socket.id
    io.in(id).emit('socketId',id);
    // store the room in a var for convenience
    var room = "room" + numb;
    //store two user in single room
    var user = [];
    // join socket to the next open room
    socket.join(room);

    // store room on socket obj for use later - IMPORTANT
    socket.current_room = room;
    // console.log('current room =',socket.current_room);

    // log some stuff for testing
    console.log("Joined room: ", socket.current_room, "users=", socket.id);

    // Check room occupancy, increment if 2 or more
    io.in(room).clients(function (err, clients) {

        console.log('user room no=', room);
        console.log('clients', clients);
        user.push(clients);
        console.log(clients.length);
        //   console.log(user.length);

        if (clients.length == 1) {
            //  var msg1 = "waiting for user for chat";
            // var obj1 = JSON.parse(msg1);
            console.log('current room no:', room, ',waiting for user for chat and current user=', clients[0]);
            io.in(socket.current_room).emit('waiting', clients);
        }
        if (clients.length == 2) {
            console.log('current room no:', room, ',connected with stranger:', clients[1]);
            io.in(socket.current_room).emit('connected', clients)
        }
        if (clients.length >= 2) {
            numb++;
        }
        console.log('----------')
    });

    socket.on('typing',function(id){
        io.to(socket.current_room).emit('typing');
    })

    socket.on('sender', function (msg,id) {
        // emit to that sockets room, now we use that current_room
        console.log(msg);
        console.log(id);
       
        io.in(socket.current_room).emit('chat message',msg,id);

    });


    socket.on('disconnect', function () {
        console.log('socket disconnected');
        console.log(room);
        var id = socket.id;

        console.log('deleted room no =', socket.current_room, 'deleted user', id);
        console.log('dleted current room no=', socket.current_room, 'remaining user in room', user[0])
        io.in(room).emit('logout');
    });
});

//--------------end random chat
console.log(config.URI)
mongoose.connect(config.URI, { useNewUrlParser: true })
    .then(() => {
        console.log("This is my database");
    }).catch((error) => {
        console.error("DB error:", error);
        process.exit(1);
    });
//app.use('/api', routes);
//app.use(errors());

//add rooms
app.post('/users', async (req, res) => {
    try {
        let payload = req.body;
        let findOne = await chat.findOne({ from: payload.from, to: payload.to });
        let findSecond = await chat.findOne({ to: payload.from, from: payload.to });

        if (!findOne) {
            if (!findSecond) {
                let createOne = await chat.create({
                    from: payload.from,
                    to: payload.to
                });
                res.status(200).json({
                    statusCode: 200,
                    message: "sucess",
                    data: createOne
                })
            } else {
                res.status(200).json({
                    statusCode: 200,
                    message: "sucess",
                    data: findSecond
                })
            }
        } else {

            res.status(200).json({
                statusCode: 200,
                message: "sucess",
                data: findOne
            })
        }

        //=============socket==================//
        let usernames = {};
        let onlineUsers = [];

        io.adapter(redis({  host: 'localhost' , port : '6379'  }));
        console.log(io.adapter(redis()));

        io.on('connection', function (socket) {
            console.log("user connected with socket_id=", socket.id);
            log.info("SOCKET ID: " + socket.id);
            socket.on('user name', function (from, room) {
                socket.username = from;
                console.log(socket.username);
                 console.log(room);
                socket.room = room;
                usernames[from] = from;
                 console.log(usernames)
                socket.join(socket.room);

               // var newUser = { id: socket.id, name: from };
               // console.log('newUsers',newUser)
               // onlineUsers.push(newUser);
               // console.log('onlineUsers',onlineUsers);

               // io.in(socket.room).emit('receiverPeer', 'SERVER', 'you have connected to room=',room);
               // io.broadcast.to(socket.room).emit('receiverPeer', 'SERVER', from + ' has connected to this room');
                //io.emit('onlineUsers', onlineUsers);

            })

            // Listen to notifyTyping event sent by client and emit a notifyTyping to the client
            socket.on('notifyTyping', function (from, to) {
                console.log('notifyTyping=',from,",",to);
                io.to(socket.room).emit('notifyTyping', from, to);
            });

            socket.on('sender', function (message) {
                console.log(message);
                console.log(io.sockets.adapter.rooms[socket.room].sockets);
                io.in(socket.room).emit('receiverPeer', message);
            })

            socket.on('disconnect', function(){
                console.log('user disconnected');
                // remove the username from global usernames list
                delete usernames[socket.username];
                console.log(usernames[socket.username])
                // update list of users in chat, client-side
            //    io.sockets.emit('updateusers', usernames);
                // echo globally that this client has left
             //   socket.broadcast.emit('receiverPeer', 'SERVER', socket.username + ' has disconnected');
                socket.leave(socket.room);

              //  io.emit('onlineUsers', onlineUsers);
               
            });
        })
//=================end socket===============

    } catch (error) {
        console.log(error);
        res.status(200).json({
            statusCode: 400,
            message: "somthing went wrong",
            data: {}
        })
    }
})

app.get('/getAllRooms', async (req, res) => {
    try {

        let find = await chat.find();
        res.status(200).json({
            statusCode: 200,
            message: "sucess",
            data: find
        })

    } catch (error) {
        res.status(200).json({
            statusCode: 400,
            message: "somthing went wrong",
            data: {}
        })

    }
})


app.post('/pushmessage', function (req, res) {

    var serverKey = 'AAAAuADXRS4:APA91bH5oSDtjOZuUS0TWobgWfpCUSifx_AZMHSMcQaVSRuHKhMdCdKE1aO9NVcIQWVak-Ce9OuFGD3euOwChnEq2-f2dVabvaJTCVdQ9f99k3_h-CAY7vwtUo0iwtyscgNAQvGyK644';
    var fcm = new FCM(serverKey);
    var reqObj = req.body;
    var token = reqObj.userToken;
    console.log("Token Value  :   " + token);
    var message = {
        to: token,
        collapse_key: 'xxxxxxxxxxxxxx',
        notification: { title: 'hello', body: 'test' },
        data: { my_key: 'my value', contents: "abcv/" }
    };
    fcm.send(message, function (err, response) {
        if (err) {
            res.json({ status: 0, message: err });
            console.log("error : " + err);
        } else {
            console.log("MESSAGE SEND");
            res.json({ status: 1, message: response });
        }
    })
});

app.use(function (req, res, next) {
    res.status(404).json({
        statusCode: 404,
        message: "Not found",
        data: {}
    })
})



http.listen(config.port, () => {
    console.log("Server is running @", config.port)
})
