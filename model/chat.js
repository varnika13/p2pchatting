/*const mongoose = require('mongoose')
const Schema = mongoose.Schema

let chatSchema = new Schema({
    
    roomId: String,
    from: String,
    to: String
})

module.exports = mongoose.model('chat', chatSchema) 
*/


var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);

var chatSchema = new Schema({
    
    roomId: String,
    from: String,
    to: String,
    history:[String],
})

chatSchema.plugin(autoIncrement.plugin,
  {
    model: 'chat',
    field: 'roomId',
    startAt: 1,
    incrementBy: 1
  });

module.exports = mongoose.model('chat', chatSchema);

