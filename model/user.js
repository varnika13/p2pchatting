const mongoose = require('mongoose')
const Schema = mongoose.Schema

let userSchema = new Schema({
    mobileNo: String,
    profilePic: String,
    coverPic:String,
   // videos: [String],
    gender: { type: String, default: null },
    fullName: String,
    dob: String,
    place: String,
    //accessToken:String,
    passcode: String,

    balance:{type:Number,default:0},
    totalCoins:{type:Number,default:0},
    
    followers: { type: Number, default: 0 },
    following: { type: Number, default: 0 },
    recieved: { type: Number, default: 0 },
    purchased: { type: Number, default: 0 },
    redeemed: { type: Number, default: 0 },
    otp: { type: Number, default: 0 },
    salt: String,
    createdAt: { type: Date, default: Date.now() }
})

module.exports = mongoose.model('user', userSchema)