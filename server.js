const app = require('express')();
const mongoose = require('mongoose')
const logger = require('morgan')
const bodyParser = require('body-parser')
//const routes = require('./routes')
const config = require('./config')
const user = require('./model/user')
const userAuth = require('./model/userAuth');
const chat = require('./model/chat');
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var FCM = require('fcm-node');

//const redis = require('socket.io-redis');

//var redis = require('redis');
//var client = '';


//const app = express();
app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

        let usernames = {};
        let onlineUsers = [];

      //  io.adapter(redis({  host: 'localhost' , port : '6379'  }));
       // console.log(io.adapter(redis()));

        io.on('connection', function (socket) {
            console.log("user connected with socket_id=", socket.id);
            socket.on('user name', function (from, room) {
                socket.username = from;
                console.log(socket.username);
                 console.log(room);
                socket.room = room;
                usernames[from] = from;
                 console.log(usernames)
                socket.join(socket.room);

            })
let rooms={}
            socket.on('sender', function (data) {
              //   console.log(data.room);
               // socket.room = data.room;
                rooms[data.from] = data.from;
                rooms[data.to]=data.to;
                 console.log(rooms)
               // socket.join(socket.room);
                socket.join(rooms);
                console.log(data.message);
                console.log(io.sockets.adapter.rooms[rooms].sockets);
                io.in(rooms).emit('receiverPeer', data.message);
            })

            socket.on('disconnect', function(){
                console.log('user disconnected');
                // remove the username from global usernames list
                delete usernames[socket.username];
                console.log(usernames[socket.username])
                // update list of users in chat, client-side
            //    io.sockets.emit('updateusers', usernames);
                // echo globally that this client has left
             //   socket.broadcast.emit('receiverPeer', 'SERVER', socket.username + ' has disconnected');
                socket.leave(socket.room);

              //  io.emit('onlineUsers', onlineUsers);
               
            });
        })
//=================end socket===============

   


    
app.use(function (req, res, next) {
    res.status(404).json({
        statusCode: 404,
        message: "Not found",
        data: {}
    })
})



http.listen(3000, () => {
    console.log("Server is running @ 3000")
})


